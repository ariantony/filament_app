<?php

namespace App\Filament\Resources\UmrahPackageResource\RelationManagers;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TimePicker;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Resources\RelationManagers\RelationManager;

class IteneraryRelationManager extends RelationManager
{
    protected static string $relationship = 'itenerary';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('module.itenerary.attribute.title_section1'))
                ->description(__('module.itenerary.attribute.desc_section1'))
                ->aside()
                ->schema([
                Forms\Components\Hidden::make('user_id')
                ->default(auth()->id()),
                Forms\Components\DatePicker::make('date')
                    ->required()
                    ->label(__('module.itenerary.field.date')),
                Forms\Components\TextInput::make('venue')
                    ->maxLength(255)
                    ->label(__('module.itenerary.field.venue')),
                Section::make()
                ->schema([
                TimePicker::make('from')
                    ->prefixIcon('heroicon-m-clock')
                    ->label(__('module.itenerary.field.from')),
                TimePicker::make('to')
                    ->prefixIcon('heroicon-m-flag')
                    ->label(__('module.itenerary.field.to')),
                ])->columns(2),
                Forms\Components\Textarea::make('activity')
                ->maxLength(255)
                ->label(__('module.itenerary.field.activity')),
                Forms\Components\Textarea::make('description')
                ->maxLength(255)
                ->label(__('module.itenerary.field.description')),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.itenerary.field.active'))
                    ->required(),
                ])
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('name')
            ->columns([
                Tables\Columns\TextColumn::make('date')
                    ->label(__('module.itenerary.field.date'))
                    ->dateTime('d F Y'),
                Tables\Columns\TextColumn::make('venue')
                    ->label(__('module.itenerary.field.venue')),
                Tables\Columns\TextColumn::make('activity')
                    ->searchable()
                    ->label(__('module.itenerary.field.activity')),
                Tables\Columns\TextColumn::make('from')
                    ->label(__('module.itenerary.field.from'))
                    ->dateTime('G:i'),
                Tables\Columns\TextColumn::make('to')
                    ->label(__('module.itenerary.field.to'))
                    ->dateTime('G:i'),
                Tables\Columns\TextColumn::make('description')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.itenerary.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->label(__('module.itenerary.field.status'))
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}
