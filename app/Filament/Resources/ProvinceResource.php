<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Province;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\ProvinceResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\ProvinceResource\RelationManagers;

class ProvinceResource extends Resource
{
    protected static ?string $model = Province::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 2;

    public static function getModelLabel(): string
    {
        return __('module.province.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.province.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.province.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Propinsi')
                ->description('Isikan data propinsi sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                    Forms\Components\Select::make('country_id')
                    ->relationship('country', 'name')
                    ->required()
                    ->label(__('module.province.field.countries_id')),
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.province.field.name')),
                Forms\Components\Textarea::make('description')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.province.field.description')),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.country.field.active'))
                    ->required(),
                ])->columns(2),
                Forms\Components\Hidden::make('user_id')
                ->default(auth()->id()),
            ])->columns(2);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')
                ->numeric()
                ->sortable()
                ->label(__('module.general.id')),
                Tables\Columns\TextColumn::make('country.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.province.field.countries_id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.province.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.province.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProvinces::route('/'),
            'create' => Pages\CreateProvince::route('/create'),
            'edit' => Pages\EditProvince::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
