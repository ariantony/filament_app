<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use App\Models\UmrahType;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\UmrahTypeResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\UmrahTypeResource\RelationManagers;

class UmrahTypeResource extends Resource
{
    protected static ?string $model = UmrahType::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 8;

    public static function getModelLabel(): string
    {
        return __('module.umrah_type.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.umrah_type.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.umrah_type.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Negara')
                ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.general.field.name')),
                Forms\Components\Textarea::make('description')
                    ->maxLength(255)
                    ->label(__('module.general.field.description')),
                Forms\Components\Hidden::make('user_id')
                    ->default(auth()->id()),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.mahram.field.active'))
                    ->required(),
                ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')
                ->label(__('module.general.field.id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('description')
                    ->searchable(),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUmrahTypes::route('/'),
            'create' => Pages\CreateUmrahType::route('/create'),
            'edit' => Pages\EditUmrahType::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
