<?php

namespace App\Filament\Resources\MahramResource\Pages;

use App\Filament\Resources\MahramResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListMahrams extends ListRecords
{
    protected static string $resource = MahramResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
