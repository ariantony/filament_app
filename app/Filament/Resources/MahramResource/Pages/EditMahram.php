<?php

namespace App\Filament\Resources\MahramResource\Pages;

use App\Filament\Resources\MahramResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMahram extends EditRecord
{
    protected static string $resource = MahramResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
