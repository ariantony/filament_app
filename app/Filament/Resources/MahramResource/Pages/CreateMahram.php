<?php

namespace App\Filament\Resources\MahramResource\Pages;

use App\Filament\Resources\MahramResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateMahram extends CreateRecord
{
    protected static string $resource = MahramResource::class;
    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
