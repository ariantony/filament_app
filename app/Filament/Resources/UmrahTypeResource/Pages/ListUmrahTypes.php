<?php

namespace App\Filament\Resources\UmrahTypeResource\Pages;

use App\Filament\Resources\UmrahTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListUmrahTypes extends ListRecords
{
    protected static string $resource = UmrahTypeResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
