<?php

namespace App\Filament\Resources\UmrahTypeResource\Pages;

use App\Filament\Resources\UmrahTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditUmrahType extends EditRecord
{
    protected static string $resource = UmrahTypeResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }
}
