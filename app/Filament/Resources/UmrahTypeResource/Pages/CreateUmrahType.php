<?php

namespace App\Filament\Resources\UmrahTypeResource\Pages;

use App\Filament\Resources\UmrahTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateUmrahType extends CreateRecord
{
    protected static string $resource = UmrahTypeResource::class;
}
