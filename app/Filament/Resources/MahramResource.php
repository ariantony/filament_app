<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Mahram;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\MahramResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\MahramResource\RelationManagers;

class MahramResource extends Resource
{
    protected static ?string $model = Mahram::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 7;

    public static function getModelLabel(): string
    {
        return __('module.mahram.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.mahram.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.mahram.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Negara')
                ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.general.field.name')),
                Forms\Components\Textarea::make('description')
                    ->maxLength(255)
                    ->label(__('module.general.field.description')),
                Forms\Components\Hidden::make('user_id')
                    ->default(auth()->id()),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.mahram.field.active'))
                    ->required(),
                ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')
                ->label(__('module.general.field.id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.general.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.general.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMahrams::route('/'),
            'create' => Pages\CreateMahram::route('/create'),
            'edit' => Pages\EditMahram::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
