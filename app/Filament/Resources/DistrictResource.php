<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\District;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\DistrictResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\DistrictResource\RelationManagers;

class DistrictResource extends Resource
{
    protected static ?string $model = District::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 4;

    public static function getModelLabel(): string
    {
        return __('module.district.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.district.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.district.navgroup');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Propinsi')
                ->description('Isikan data propinsi sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                    Forms\Components\Select::make('country_id')
                    ->relationship('country', 'name')
                    ->label(__('module.district.field.countries_id'))
                    ->required(),
                    Forms\Components\Select::make('province_id')
                        ->relationship('province', 'name')
                        ->required()
                        ->label(__('module.district.field.provinces_id')),
                    Forms\Components\Select::make('kabukot_id')
                        ->relationship('kabukot', 'name')
                        ->required()
                        ->label(__('module.district.field.kabukot_id')),
                    Forms\Components\Hidden::make('user_id')
                        ->default(auth()->id()),
                    Forms\Components\TextInput::make('name')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.district.field.name')),
                    Forms\Components\Textarea::make('description')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.district.field.description')),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.country.field.active'))
                    ->required(),
                ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                ->numeric()
                ->sortable()
                ->label(__('module.general.id')),
                Tables\Columns\TextColumn::make('country.name')
                    ->sortable()
                    ->label(__('module.district.field.countries_id')),
                Tables\Columns\TextColumn::make('province.name')
                    ->sortable()
                    ->label(__('module.district.field.provinces_id')),
                Tables\Columns\TextColumn::make('kabukot.name')
                    ->sortable()
                    ->label(__('module.district.field.kabukot_id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.district.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.district.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDistricts::route('/'),
            'create' => Pages\CreateDistrict::route('/create'),
            'edit' => Pages\EditDistrict::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
