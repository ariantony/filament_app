<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\Ward;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\WardResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\WardResource\RelationManagers;

class WardResource extends Resource
{
    protected static ?string $model = Ward::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 5;

    public static function getModelLabel(): string
    {
        return __('module.ward.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.ward.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.ward.navgroup');
    }


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Propinsi')
                ->description('Isikan data propinsi sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                Forms\Components\Select::make('country_id')
                    ->relationship('country', 'name')
                    ->required()
                    ->label(__('module.ward.field.country_id')),
                Forms\Components\Select::make('province_id')
                    ->relationship('province', 'name')
                    ->required()
                    ->label(__('module.ward.field.province_id')),
                Forms\Components\Select::make('kabukot_id')
                    ->relationship('kabukot', 'name')
                    ->required()
                    ->label(__('module.ward.field.kabukot_id')),
                Forms\Components\Select::make('district_id')
                    ->relationship('district', 'name')
                    ->required()
                    ->label(__('module.ward.field.district_id')),
                Forms\Components\Hidden::make('user_id')
                    ->default(auth()->id()),
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.ward.field.name')),
                Forms\Components\Textarea::make('description')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.ward.field.description'))
                    ->columnSpanFull(),
                ])->columns(2),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.country.field.active'))
                    ->required(),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                ->numeric()
                ->sortable()
                ->label(__('module.general.id')),
                Tables\Columns\TextColumn::make('country.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.ward.field.country_id')),
                Tables\Columns\TextColumn::make('province.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.ward.field.province_id')),
                Tables\Columns\TextColumn::make('kabukot.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.ward.field.kabukot_id')),
                Tables\Columns\TextColumn::make('district.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.ward.field.district_id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.ward.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.ward.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListWards::route('/'),
            'create' => Pages\CreateWard::route('/create'),
            'edit' => Pages\EditWard::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
