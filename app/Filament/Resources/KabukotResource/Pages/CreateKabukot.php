<?php

namespace App\Filament\Resources\KabukotResource\Pages;

use App\Filament\Resources\KabukotResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateKabukot extends CreateRecord
{
    protected static string $resource = KabukotResource::class;

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
