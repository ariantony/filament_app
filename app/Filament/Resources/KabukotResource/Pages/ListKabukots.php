<?php

namespace App\Filament\Resources\KabukotResource\Pages;

use App\Filament\Resources\KabukotResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListKabukots extends ListRecords
{
    protected static string $resource = KabukotResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
