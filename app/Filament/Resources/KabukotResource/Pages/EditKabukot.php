<?php

namespace App\Filament\Resources\KabukotResource\Pages;

use App\Filament\Resources\KabukotResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditKabukot extends EditRecord
{
    protected static string $resource = KabukotResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\ForceDeleteAction::make(),
            Actions\RestoreAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
}
