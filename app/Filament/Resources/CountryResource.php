<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Country;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\CountryResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\CountryResource\RelationManagers;

class CountryResource extends Resource
{
    protected static ?string $model = Country::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 1;

    public static function getModelLabel(): string
    {
        return __('module.country.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.country.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.country.navgroup');
    }

    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Section::make('Data Master Negara')
            ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
            ->aside()
            ->schema([
            Forms\Components\TextInput::make('name')
                ->required()
                ->label(__('module.country.field.name'))
                ->maxLength(255)
                ->columnSpanFull(),
            Forms\Components\Section::make(__('module.country.attribute.section'))
            ->description(__('module.country.attribute.dessection'))
            ->schema([
                Forms\Components\TextInput::make('code1')
                ->label(__('module.country.field.code1'))
                ->maxLength(255),
                Forms\Components\TextInput::make('code2')
                    ->maxLength(255)
                    ->label(__('module.country.field.code2')),
                Forms\Components\TextInput::make('code3')
                    ->maxLength(255)
                    ->label(__('module.country.field.code3')),
                Forms\Components\Hidden::make('user_id')
                    ->default(auth()->id()),
            ])->columns(3),
            Forms\Components\Textarea::make('description')
            ->maxLength(255)
            ->label(__('module.country.field.description'))
            ->columnSpanFull(),
            ]),
            Section::make('Status')
            ->schema([
                Forms\Components\Toggle::make('status')
                ->label(__('module.country.field.active'))
                ->required(),
            ])->columns(2),
        ])->columns(2);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                ->numeric()
                ->sortable()
                ->label(__('module.general.id')),
                Tables\Columns\TextColumn::make('name')
                ->label(__('module.country.field.name'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('code1')
                    ->searchable()
                    ->label(__('module.country.field.code1')),
                Tables\Columns\TextColumn::make('code2')
                    ->searchable()
                    ->label(__('module.country.field.code2')),
                Tables\Columns\TextColumn::make('code3')
                    ->searchable()
                    ->label(__('module.country.field.code3')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.country.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCountries::route('/'),
            'create' => Pages\CreateCountry::route('/create'),
            'edit' => Pages\EditCountry::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
