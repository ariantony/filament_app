<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Jamaah;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\JamaahResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\JamaahResource\RelationManagers;

class JamaahResource extends Resource
{
    protected static ?string $model = Jamaah::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 1;

    public static function getModelLabel(): string
    {
        return __('module.jamaah.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.jamaah.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.jamaah.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('module.itenerary.attribute.title_section1'))
                ->description(__('module.itenerary.attribute.desc_section1'))
                ->aside()
                ->schema([
                    Section::make('Identitas')
                    ->schema([
                        Forms\Components\TextInput::make('first_name')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.jamaah.field.first_name')),
                    Forms\Components\TextInput::make('middle_name')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.jamaah.field.middle_name')),
                    Forms\Components\TextInput::make('last_name')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.jamaah.field.last_name')),
                    Forms\Components\Select::make('gender_id')
                        ->relationship('gender', 'name')
                        ->required()
                        ->columnSpan(1)
                        ->label(__('module.jamaah.field.gender_id')),
                    Forms\Components\DatePicker::make('dateofborn')
                        ->required()->columnSpan(1)
                        ->label(__('module.jamaah.field.dateofborn')),
                    Forms\Components\TextInput::make('phone_number')
                        ->tel()
                        ->required()
                        ->maxLength(255)
                        ->columnSpan(1)
                        ->label(__('module.jamaah.field.phone_number')),
                    Forms\Components\Select::make('marital_status_id')
                        ->relationship('maritalStatus', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.marital_status_id')),
                    Forms\Components\Select::make('blood_type_id')
                        ->relationship('bloodType', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.blood_type_id')),
                    ])->columns(2),
                    Section::make('Alamat')
                    ->schema([
                        Forms\Components\Select::make('country_id')
                        ->relationship('country', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.country_id')),
                    Forms\Components\Select::make('province_id')
                        ->relationship('province', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.province_id')),
                    Forms\Components\Select::make('kabukot_id')
                        ->relationship('kabukot', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.kabukot_id')),
                    Forms\Components\Select::make('district_id')
                        ->relationship('district', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.district_id')),
                    Forms\Components\Select::make('ward_id')
                        ->relationship('ward', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.ward_id')),
                    Forms\Components\Textarea::make('address')
                        ->required()
                        ->maxLength(255)
                        ->columnSpanFull()
                        ->label(__('module.jamaah.field.address')),
                    ])->columns(2),
                    Section::make()
                    ->schema([
                        Forms\Components\Select::make('mahram_id')
                        ->relationship('mahram', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.mahram_id')),
                    Forms\Components\Select::make('umrah_package_id')
                        ->relationship('umrahPackage', 'name')
                        ->required()
                        ->label(__('module.jamaah.field.umrah_package_id')),
                    ]),
                    Forms\Components\Textarea::make('desciption')
                    ->required()
                    ->maxLength(255)
                    ->label(__('module.jamaah.field.description')),
                ]),
                Forms\Components\Hidden::make('user_id')
                    ->default(auth()->id()),
                Section::make('Status')
                    ->schema([
                        Forms\Components\Toggle::make('status')
                        ->label(__('module.jamaah.field.status'))
                        ->required(),
                    ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('first_name')
                    ->searchable()
                    ->sortable()
                    ->label(__('module.jamaah.field.first_name')),
                Tables\Columns\TextColumn::make('middle_name')
                    ->searchable()
                    ->sortable()
                    ->label(__('module.jamaah.field.middle_name')),
                Tables\Columns\TextColumn::make('last_name')
                    ->searchable()
                    ->sortable()
                    ->label(__('module.jamaah.field.last_name')),
                Tables\Columns\TextColumn::make('gender.name')
                    ->label(__('module.jamaah.field.gender_id')),
                Tables\Columns\TextColumn::make('mahram.name')
                    ->label(__('module.jamaah.field.mahram_id')),
                Tables\Columns\TextColumn::make('maritalStatus.name')
                    ->label(__('module.jamaah.field.marital_status_id')),
                Tables\Columns\TextColumn::make('country.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.country_id')),
                Tables\Columns\TextColumn::make('province.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.province_id')),
                Tables\Columns\TextColumn::make('kabukot.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.kabukot_id')),
                Tables\Columns\TextColumn::make('district.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.district_id')),
                Tables\Columns\TextColumn::make('ward.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.ward_id')),
                Tables\Columns\TextColumn::make('umrahPackage.name')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.umrah_package_id')),
                Tables\Columns\TextColumn::make('dateofborn')
                    ->date('d M Y')
                    ->label(__('module.jamaah.field.dateofborn')),
                Tables\Columns\TextColumn::make('phone_number')
                    ->dateTime('d F Y')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.phone_number')),
                Tables\Columns\TextColumn::make('address')
                    ->searchable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.address')),
                Tables\Columns\TextColumn::make('desciption')
                    ->searchable()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label(__('module.jamaah.field.status')),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListJamaahs::route('/'),
            'create' => Pages\CreateJamaah::route('/create'),
            'edit' => Pages\EditJamaah::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
