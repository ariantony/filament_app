<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Kabukot;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\KabukotResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\KabukotResource\RelationManagers;

class KabukotResource extends Resource
{
    protected static ?string $model = Kabukot::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 3;

    public static function getModelLabel(): string
    {
        return __('module.kabukot.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.kabukot.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.kabukot.navgroup');
    }

    public static function form(Form $form): Form
    {
        return $form
        ->schema([
            Section::make('Data Master Propinsi')
            ->description('Isikan data propinsi sesuai dengan negara yang dipilih.')
            ->aside()
            ->schema([
                Forms\Components\Select::make('country_id')
                    ->relationship('country', 'name')
                    ->required()
                    ->label(__('module.kabukot.field.countries_id')),
                Forms\Components\Select::make('province_id')
                    ->relationship('province', 'name')
                    ->required()
                    ->label(__('module.kabukot.field.provinces_id')),
            Forms\Components\TextInput::make('name')
                ->required()
                ->maxLength(255)
                ->label(__('module.kabukot.field.name')),
            Forms\Components\Textarea::make('description')
                ->required()
                ->maxLength(255)
                ->label(__('module.kabukot.field.description')),
            ]),
            Section::make('Status')
            ->schema([
                Forms\Components\Toggle::make('status')
                ->label(__('module.country.field.active'))
                ->required(),
            ])->columns(2),
            Forms\Components\Hidden::make('user_id')
            ->default(auth()->id()),
        ])->columns(2);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.general.id')),
                TextColumn::make('country.name')
                    ->sortable()
                    ->label(__('module.kabukot.field.countries_id')),
                TextColumn::make('province.name')
                    ->sortable()
                    ->label(__('module.kabukot.field.provinces_id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.kabukot.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.kabukot.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                    Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListKabukots::route('/'),
            'create' => Pages\CreateKabukot::route('/create'),
            'edit' => Pages\EditKabukot::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
