<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Gender;
use Filament\Forms\Form;
use Filament\Tables\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Illuminate\Database\Eloquent\Builder;
use App\Filament\Resources\GenderResource\Pages;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\GenderResource\RelationManagers;
use Filament\Tables\Columns\TextColumn;

class GenderResource extends Resource
{
    protected static ?string $model = Gender::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 6;

    public static function getModelLabel(): string
    {
        return __('module.gender.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.gender.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.gender.navgroup');
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Negara')
                ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                    TextInput::make('name')
                        ->required()
                        ->label(__('module.general.field.name'))
                        ->maxLength(255)
                        ->columnSpanFull(),
                    Textarea::make('description')
                        ->maxLength(255)
                        ->label(__('module.general.field.description'))
                        ->columnSpanFull(),
                    Hidden::make('user_id')->default(auth()->id()),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.country.field.active'))
                    ->required(),
                ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                ->sortable()
                ->label(__('module.general.field.id')),
                TextColumn::make('name')
                ->searchable(),
                TextColumn::make('description')
                ->searchable()
                ->label(__('module.country.field.description')),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGenders::route('/'),
            'create' => Pages\CreateGender::route('/create'),
            'edit' => Pages\EditGender::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
