<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use App\Models\MaritalStatus;
use Filament\Resources\Resource;
use Filament\Forms\Components\Hidden;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\MaritalStatusResource\Pages;
use App\Filament\Resources\MaritalStatusResource\RelationManagers;

class MaritalStatusResource extends Resource
{
    protected static ?string $model = MaritalStatus::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 6;

    public static function getModelLabel(): string
    {
        return __('module.marital_status.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.marital_status.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.marital_status.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Negara')
                ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                    TextInput::make('name')
                        ->required()
                        ->label(__('module.marital_status.field.name'))
                        ->maxLength(255)
                        ->columnSpanFull(),
                    Textarea::make('description')
                        ->maxLength(255)
                        ->label(__('module.general.field.description'))
                        ->columnSpanFull(),
                    Hidden::make('user_id')->default(auth()->id()),
                ]),
                Section::make('Status')
                ->schema([
                    Forms\Components\Toggle::make('status')
                    ->label(__('module.country.field.active'))
                    ->required(),
                ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')
                    ->sortable()
                    ->label(__('module.general.field.id')),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->label(__('module.marital_status.field.name')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.general.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMaritalStatuses::route('/'),
            'create' => Pages\CreateMaritalStatus::route('/create'),
            'edit' => Pages\EditMaritalStatus::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
