<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use App\Models\UmrahPackage;
use Filament\Resources\Resource;
use Filament\Forms\Components\Section;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use App\Filament\Resources\UmrahPackageResource\Pages;
use App\Filament\Resources\UmrahPackageResource\RelationManagers;

class UmrahPackageResource extends Resource
{
    protected static ?string $model = UmrahPackage::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?int $navigationSort = 9;

    public static function getModelLabel(): string
    {
        return __('module.umrah_package.modellabel');
    }

    public static function getNavigationLabel(): string
    {
        return __('module.umrah_package.navlabel');
    }

    public static function getNavigationGroup(): ?string
    {
        return __('module.umrah_package.navgroup');
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Data Master Negara')
                ->description('Isikan data Negara sesuai dengan negara yang dipilih.')
                ->aside()
                ->schema([
                    Forms\Components\Select::make('umrah_type_id')
                        ->relationship('umrahType', 'name')
                        ->required()
                        ->label(__('module.umrah_package.field.umrah_type_id')),
                    Forms\Components\Hidden::make('user_id')
                            ->default(auth()->id()),
                    Forms\Components\TextInput::make('name')
                        ->required()
                        ->maxLength(255)
                        ->label(__('module.umrah_package.field.name')),
                    Section::make()
                    ->schema([
                    Forms\Components\DatePicker::make('from')
                        ->label(__('module.umrah_package.field.from')),
                    Forms\Components\DatePicker::make('to')
                        ->label(__('module.umrah_package.field.to')),
                    ])->columns(2),
                    Forms\Components\Textarea::make('description')
                        ->maxLength(255)
                        ->label(__('module.umrah_package.field.description')),
                    Section::make('Status')
                        ->schema([
                            Forms\Components\Toggle::make('status')
                                ->label(__('module.umrah_package.field.active'))
                                ->required(),
                        ])
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                ->searchable()
                ->label(__('module.umrah_package.field.name')),
                Tables\Columns\TextColumn::make('umrahType.name')
                    ->numeric()
                    ->sortable()
                    ->label(__('module.umrah_package.field.umrah_type_id')),
                Tables\Columns\TextColumn::make('from')
                    ->date()
                    ->sortable()
                    ->label(__('module.umrah_package.field.from')),
                Tables\Columns\TextColumn::make('to')
                    ->date()
                    ->sortable()
                    ->label(__('module.umrah_package.field.to')),
                Tables\Columns\TextColumn::make('description')
                    ->searchable()
                    ->label(__('module.umrah_package.field.description')),
                Tables\Columns\IconColumn::make('status')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    Tables\Actions\ForceDeleteBulkAction::make(),
                    Tables\Actions\RestoreBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\IteneraryRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUmrahPackages::route('/'),
            'create' => Pages\CreateUmrahPackage::route('/create'),
            'edit' => Pages\EditUmrahPackage::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
