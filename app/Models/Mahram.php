<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mahram extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function jamaah(): HasMany {
        return $this->hasMany(Jamaah::class);
    }
}
