<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Province extends Model
{
    use SoftDeletes ;

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function kabukot(): HasMany
    {
        return $this->hasMany(Kabukot::class);
    }

    public function district(): HasMany
    {
        return $this->hasMany(District::class);
    }

    public function ward(): HasMany {
        return $this->hasMany(Ward::class);
    }

    public function jamaah(): HasMany {
        return $this->hasMany(Jamaah::class);
    }

}
