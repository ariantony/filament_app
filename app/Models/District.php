<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class District extends Model
{
    use HasFactory;
    use SoftDeletes;


    public function kabukot(): BelongsTo
    {
        return $this->belongsTo(Kabukot::class);
    }

    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class);
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function ward(): HasMany {
        return $this->hasMany(Ward::class);
    }

    public function jamaah(): HasMany {
        return $this->hasMany(Jamaah::class);
    }
}
