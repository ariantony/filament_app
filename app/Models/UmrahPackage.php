<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UmrahPackage extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function umrahType(): BelongsTo
    {
        return $this->belongsTo(UmrahType::class);
    }

    public function itenerary(): HasMany {
        return $this->hasMany(Itenerary::class);
    }

    public function jamaah(): HasMany {
        return $this->hasMany(Jamaah::class);
    }
}

