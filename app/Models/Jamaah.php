<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jamaah extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }
    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class);
    }
    public function kabukot(): BelongsTo
    {
        return $this->belongsTo(Kabukot::class);
    }
    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class);
    }
    public function ward(): BelongsTo
    {
        return $this->belongsTo(Ward::class);
    }
    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class);
    }
    public function mahram(): BelongsTo
    {
        return $this->belongsTo(Mahram::class);
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function umrahPackage(): BelongsTo
    {
        return $this->belongsTo(UmrahPackage::class);
    }
    public function bloodType(): BelongsTo
    {
        return $this->belongsTo(BloodType::class);
    }
    public function maritalStatus(): BelongsTo
    {
        return $this->belongsTo(MaritalStatus::class);
    }
}
