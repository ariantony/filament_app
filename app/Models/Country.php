<?php

namespace App\Models;

use App\Models\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Country extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function province(): HasMany
    {
        return $this->hasMany(Province::class);
    }

    public function kabukot(): HasMany
    {
        return $this->hasMany(Kabukot::class);
    }

    public function district(): HasMany
    {
        return $this->hasMany(District::class);
    }

    public function ward(): HasMany {
        return $this->hasMany(Ward::class);
    }

    public function jamaah(): HasMany {
        return $this->hasMany(Jamaah::class);
    }

}
