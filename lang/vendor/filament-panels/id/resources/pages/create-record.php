<?php

return [

    'title' => 'Tambah data :label',

    'breadcrumb' => 'Tambah data',

    'form' => [

        'actions' => [

            'cancel' => [
                'label' => 'Batal',
            ],

            'create' => [
                'label' => 'Simpan',
            ],

            'create_another' => [
                'label' => 'Tambah data lainnya',
            ],

        ],

    ],

    'notifications' => [

        'created' => [
            'title' => 'Data berhasil dibuat',
        ],

    ],

];
