<?php

return [
    'general' =>[
        'id' => 'ID',
    ],
    'country' => [
        'navlabel' => 'Country',
        'navgroup' => 'Setting & Configuration',
        'modellabel' => 'Country',
        'field' =>[
            'name' => 'Country Name',
            'code1' => 'Code 1',
            'code2' => 'Code 2',
            'code3' => 'Code 3',
            'description' => 'Description',
            'active' => 'Non Active/Active',
        ],
        'attribute' => [
            'section' => 'Country Code',
            'dessection' => 'Entry Country Code',
        ]
    ],
    'province' => [
        'navlabel' => 'Province',
        'navgroup' => 'Setting & Configuration',
        'modellabel' => 'Province',
        'field' =>[
            'name' => 'Province Name',
            'countries_id' => 'Country',
            'description' => 'Description',
            'active' => 'Non Active/Active',
        ]
    ],
    'kabukot' => [
        'navlabel' => 'Regency',
        'navgroup' => 'Setting & Configuration',
        'modellabel' => 'Regency',
        'field' =>[
            'name' => 'Regency',
            'countries_id' => 'Country',
            'provinces_id' => 'Province',
            'description' => 'Description',
            'active' => 'Non Active/Active',
        ]
    ],

];
