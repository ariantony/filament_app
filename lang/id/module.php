<?php

return [
    'general' =>[
        'id' => 'ID',
        'attribute' => [
            'title_section' => 'Kode Negara',
            'desc_section' => 'Isikan kode negara',
        ],
        'field' => [
            'id' => '#',
            'name' => 'Nama',
            'description' => 'Keterangan',
        ],
    ],
    'country' => [
        'navlabel' => 'Negara',
        'navgroup' => 'Setting & Konfigurasi',
        'modellabel' => 'Negara',
        'field' =>[
            'name' => 'Nama Negara',
            'code1' => 'Kode 1',
            'code2' => 'Kode 2',
            'code3' => 'Kode 3',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ],
        'attribute' => [
            'section' => 'Kode Negara',
            'dessection' => 'Isikan kode negara',
        ]
    ],
    'province' => [
        'navlabel' => 'Propinsi',
        'navgroup' => 'Setting & Konfigurasi',
        'modellabel' => 'Propinsi',
        'field' =>[
            'name' => 'Nama Propinsi',
            'countries_id' => 'Negara',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'kabukot' => [
        'navlabel' => 'Kabukaten/Kota',
        'navgroup' => 'Setting & Konfigurasi',
        'modellabel' => 'Kabukaten/Kota',
        'field' =>[
            'name' => 'Kabukaten/Kota',
            'countries_id' => 'Negara',
            'provinces_id' => 'Propinsi',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'district' => [
        'navlabel' => 'Kecamatan',
        'navgroup' => 'Setting & Konfigurasi',
        'modellabel' => 'Kecamatan',
        'field' =>[
            'name' => 'Kecamatan',
            'countries_id' => 'Negara',
            'provinces_id' => 'Propinsi',
            'kabukot_id' => 'Kabukaten/Kota',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'ward' => [
        'navlabel' => 'Kelurahan',
        'navgroup' => 'Setting & Konfigurasi',
        'modellabel' => 'Kelurahan',
        'field' =>[
            'name' => 'Kelurahan',
            'country_id' => 'Negara',
            'province_id' => 'Propinsi',
            'kabukot_id' => 'Kabukaten/Kota',
            'district_id' => 'Kecamatan',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'gender' => [
        'navlabel' => 'Gender',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Gender',
        'field' =>[
            'id' => '#',
            'name' => 'Gender',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'mahram' => [
        'navlabel' => 'Mahram',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Mahram',
        'field' =>[
            'id' => '#',
            'name' => 'Mahram',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'umrah_type' => [
        'navlabel' => 'Jenis Paket Umroh',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Jenis Paket Umroh',
        'field' =>[
            'id' => '#',
            'name' => 'Jenis Paket Umroh',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'umrah_package' => [
        'navlabel' => 'Paket Umroh',
        'navgroup' => 'Modul Utama',
        'modellabel' => 'Paket Umroh',
        'field' =>[
            'id' => '#',
            'umrah_type_id' => 'Jenis Paket Umrah',
            'name' => 'Paket Umroh',
            'from' => 'Dari',
            'to' => 'Hingga',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
        ]
    ],
    'itenerary' => [
        'navlabel' => 'Paket Umroh',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Paket Umroh',
        'field' =>[
            'id' => '#',
            'date' => 'Tanggal',
            'venue' => 'Lokasi',
            'from'=> 'Dari',
            'to'=> 'Hingga',
            'activity'=> 'Aktivitas',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
            'status' => 'Non Aktif/Aktif',
        ],
        'attribute' => [
            'title_section1' => 'Membuat Data Itenerary',
            'desc_section1' => 'Isikan Itenerary berdasarkan lokasi, tanggal dan aktivitas',
        ],
    ],
    'marital_status' => [
        'navlabel' => 'Status Perkawinan',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Status Perkawinan',
        'field' =>[
            'id' => '#',
            'name' => 'Status Perkawinan',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
            'status' => 'Non Aktif/Aktif',
        ],
        'attribute' => [
            'title_section1' => 'Membuat Data Itenerary',
            'desc_section1' => 'Isikan Itenerary berdasarkan lokasi, tanggal dan aktivitas',
        ],
    ],
    'blood_type' => [
        'navlabel' => 'Golongan Darah',
        'navgroup' => 'Setting Umum',
        'modellabel' => 'Golongan Darah',
        'field' =>[
            'id' => '#',
            'name' => 'Golongan Darah',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
            'status' => 'Non Aktif/Aktif',
        ],
        'attribute' => [
            'title_section1' => 'Membuat Data Itenerary',
            'desc_section1' => 'Isikan Itenerary berdasarkan lokasi, tanggal dan aktivitas',
        ],
    ],
    'jamaah' => [
        'navlabel' => 'Data Jamaah',
        'navgroup' => 'Modul Utama',
        'modellabel' => 'Jamaah',
        'field' =>[
            'id' => '#',
            'first_name' => 'Nama Depan',
            'middle_name' => 'Nama Tengah',
            'last_name' => 'Nama Belakang',
            'gender_id' => 'Jenis Kelamin',
            'dateofborn' => 'Tanggal Lahir',
            'mahram_id' => 'Mahrom',
            'phone_number' => 'No. Telepon/HP',
            'marital_status_id' => 'Status Perkawinan',
            'blood_type_id' => 'Golongan Darah',
            'country_id' => 'Negara',
            'province_id' => 'Propinsi',
            'kabukot_id' => 'Kabupaten/Kota',
            'district_id' => 'Kecamatan',
            'ward_id' => 'Kelurahan',
            'address' => 'Alamat Lengkap',
            'umrah_package_id' => 'Paket Umroh',
            'description' => 'Keterangan',
            'active' => 'Non Aktif/Aktif',
            'status' => 'Non Aktif/Aktif',
        ],
        'attribute' => [
            'title_section1' => 'Membuat Data Itenerary',
            'desc_section1' => 'Isikan Itenerary berdasarkan lokasi, tanggal dan aktivitas',
        ],
    ],
];
