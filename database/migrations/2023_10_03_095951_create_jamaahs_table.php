<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jamaahs', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->foreignId('gender_id')->constrained('genders')->cascadeOnDelete();
            $table->date('dateofborn');
            $table->foreignId('mahram_id')->constrained('mahrams')->cascadeOnDelete();
            $table->string('phone_number');
            $table->foreignId('country_id')->constrained('countries')->cascadeOnDelete();
            $table->foreignId('province_id')->constrained('provinces')->cascadeOnDelete();
            $table->foreignId('kabukot_id')->constrained('kabukots')->cascadeOnDelete();
            $table->foreignId('district_id')->constrained('districts')->cascadeOnDelete();
            $table->foreignId('ward_id')->constrained('wards')->cascadeOnDelete();
            $table->string('address');
            $table->foreignId('umrah_package_id')->constrained('umrah_packages')->cascadeOnDelete();
            $table->string('desciption');
            $table->boolean('status');
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jamaahs');
    }
};
