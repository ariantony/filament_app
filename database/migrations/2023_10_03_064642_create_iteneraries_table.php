<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('iteneraries', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->foreignId('umrah_package_id')->constrained('umrah_packages')->cascadeOnDelete();
            $table->time('from')->nullable();
            $table->time('to')->nullable();
            $table->string('activity')->nullable();
            $table->string('description')->nullable();
            $table->boolean('status')->default('0');
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('iteneraries');
    }
};
